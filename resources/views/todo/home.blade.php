@extends('layout.app')

@section('body')
  <br>
  <a href="todo/create" class="btn btn-info">Add new</a>
  <div class="col-lg-6 col-lg-offset-3">
    <center><h1>Todo List</h1></center>
    <ul class="list-group col-lg-8">
        @foreach ($todos as $todo)
          <li class="list-group-item">
            <a href="{{'todo/'.$todo->id}}">{{$todo->title}}</a>
            <span class="pull-right">{{$todo->created_at->diffForHumans()}}</span>
          </li>
        @endforeach
    </ul>

    <ul class="list-group col-lg-4">
        @foreach ($todos as $todo)
          <li class="list-group-item">
            <a href="{{'todo/'.$todo->id.'/edit'}}">Edit</a>
            <form class="form-group pull-right" action="{{'todo/'.$todo->id}}" method="post">
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">  <!-- チュートリアルにない記述。 TokenMismatchException対策でｇｇった -->
                    {{method_field('DELETE')}}
              <button type="submit" style="border:none">テスト</button>
            </form>
          </li>
        @endforeach
    </ul>
  </div>
@endsection
