<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {  //http://192.168.10.10:8000/
    return view('welcome');
});


Route::get('aa', function () {  //http://192.168.10.10:8000/aa
    return "aaやった";
});

Route::resource('todo','todocontroller');//http://192.168.10.10:8000/
